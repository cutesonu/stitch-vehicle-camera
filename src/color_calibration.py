import cv2
import os
import sys
import random
import numpy as np


from utils.constants import SRC_HEIGHT, SRC_WIDTH, N_SAMPLES, DEBUG_DIR
from src.homograph import HomographMatrix


class ColorCalibration:
    def __init__(self, warp_size, m_cylinder_model=None, debug=False):
        self.color_ratios = np.zeros((2, 3), dtype=np.float32)

        if m_cylinder_model is not None:
            self.calib_model = m_cylinder_model
            self.b_pre_calib = True
        else:
            self.calib_model = None
            self.b_pre_calib = False

        self.homograph = HomographMatrix(debug=debug)

        self.src_hegiht = SRC_HEIGHT
        self.src_width = SRC_WIDTH
        self.n_samples = N_SAMPLES
        self.warp_size = warp_size.copy()
        self.debug = debug

    def generate_overlap_mask(self, l_h_mat, r_h_mat):
        # create the np.ones
        ones = np.ones((self.src_hegiht, self.src_width, 3), dtype=np.uint8) * 255
        if self.b_pre_calib:
            # camera distort
            ones_calib_left = cv2.remap(src=ones, map1=self.calib_model['map1'], map2=self.calib_model['map2'],
                                        interpolation=cv2.INTER_LINEAR)
            ones_calib_right = cv2.remap(src=ones, map1=self.calib_model['map1'], map2=self.calib_model['map2'],
                                         interpolation=cv2.INTER_LINEAR)
        else:
            ones_calib_left = ones.copy()
            ones_calib_right = ones.copy()

        # warpping
        ones_warp_left = self.homograph.transform(img=ones_calib_left, matrix=l_h_mat,
                                                  dst_sz=(self.warp_size['width'], self.warp_size['height']))
        ones_warp_right = self.homograph.transform(img=ones_calib_right, matrix=r_h_mat,
                                                   dst_sz=(self.warp_size['width'], self.warp_size['height']))

        # create binary image
        _, left_mask = cv2.threshold(ones_warp_left, 1, 255, cv2.THRESH_BINARY)
        _, right_mask = cv2.threshold(ones_warp_right, 1, 255, cv2.THRESH_BINARY)

        overlap_mask = cv2.bitwise_and(left_mask, right_mask)

        if self.debug:
            cv2.imwrite(os.path.join(DEBUG_DIR, "overlap.jpg"), overlap_mask)

        return overlap_mask

    def calculate_color_calib_ratio(self, left_paths, right_paths, l_h_mat, r_h_mat):
        overlap_mask = self.generate_overlap_mask(l_h_mat=l_h_mat, r_h_mat=r_h_mat)

        n_samples = min(self.n_samples, len(left_paths), len(right_paths))
        _min_sz = min(len(left_paths), len(right_paths))
        total_ids = list(range(_min_sz))
        random.shuffle(total_ids)
        sample_ids = total_ids[:n_samples]

        for _idx, i in enumerate(sample_ids):
            sys.stdout.write("\r\t\t{}/{}".format(_idx + 1, n_samples))
            sys.stdout.flush()
            left_path = left_paths[i]
            right_path = right_paths[i]

            left_img = cv2.imread(left_path)
            right_img = cv2.imread(right_path)

            if self.b_pre_calib:
                # camera distort
                left_calib = cv2.remap(src=left_img, map1=self.calib_model['map1'], map2=self.calib_model['map2'],
                                       interpolation=cv2.INTER_LINEAR)
                right_calib = cv2.remap(src=right_img, map1=self.calib_model['map1'], map2=self.calib_model['map2'],
                                        interpolation=cv2.INTER_LINEAR)
            else:
                left_calib = left_img.copy()
                right_calib = right_img.copy()

            # warpping
            left_warp = self.homograph.transform(img=left_calib, matrix=l_h_mat,
                                                 dst_sz=(self.warp_size['width'], self.warp_size['height']))
            left_overlap = cv2.bitwise_and(left_warp, overlap_mask)

            right_warp = self.homograph.transform(img=right_calib, matrix=r_h_mat,
                                                  dst_sz=(self.warp_size['width'], self.warp_size['height']))
            right_overlap = cv2.bitwise_and(right_warp, overlap_mask)

            if self.debug:
                cv2.imwrite(os.path.join(DEBUG_DIR, "left_overlap.jpg"), left_overlap)
                cv2.imwrite(os.path.join(DEBUG_DIR, "right_overlap.jpg"), right_overlap)
            #
            ratios = self.compare_histogram(l_img=left_warp, r_img=right_warp)

            #
            self.color_ratios += ratios

        print()
        self.color_ratios /= n_samples
        return self.color_ratios

    @staticmethod
    def compare_histogram(l_img, r_img):

        if len(l_img.shape) == 3 and l_img.shape[2] == 3:
            num_channels = 3  # number of channels : R G B
        else:
            sys.stderr.write("Support for only RGB color video.\n")
            sys.exit(1)

        ratios = np.ones((2, num_channels), dtype=np.float32)

        left_thresh = cv2.threshold(l_img, 1, 255, cv2.THRESH_BINARY)[1]
        right_thresh = cv2.threshold(r_img, 1, 255, cv2.THRESH_BINARY)[1]

        dupli_mask = cv2.bitwise_and(left_thresh, right_thresh)

        dupli_mask = (dupli_mask / 255).astype(np.uint8)
        left_dupli = l_img * dupli_mask
        right_dupli = r_img * dupli_mask

        for ch in range(num_channels):
            hist_left = cv2.calcHist([left_dupli], [ch], None, [256], [0, 256])
            hist_right = cv2.calcHist([right_dupli], [ch], None, [256], [0, 256])
            avg_left = 0.0
            avg_right = 0.0
            sum_pixels = 0.0
            for i in range(1, 256):
                avg_left += hist_left[i] * i
                avg_right += hist_right[i] * i
                sum_pixels += hist_left[i]

            avg_left /= sum_pixels
            avg_right /= sum_pixels

            ratio = avg_left / avg_right
            if ratio < 1.0:
                ratios[1][ch] = ratio
            else:
                ratios[0][ch] = 1.0 / ratio
        return ratios
