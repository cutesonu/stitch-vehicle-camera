import cv2
import sys
import os
import time
import math
import numpy as np

from utils.constants import DEBUG_DIR, MATCHING_MODE, PERSPECTIVE_MATCH, AFFINE_MATCH, MATCH_METHOD, GOOD_MATCH_THRESH


class HomographMatrix:
    def __init__(self, debug=False):
        self.debug = debug

        # surf/sift
        # self.surf = cv2.xfeatures2d.SURF_create()
        self.surf = cv2.xfeatures2d.SIFT_create()
        index_params = dict(algorithm=1, tree=5)  # FLANN_INDEX_KDTREE = 0
        search_params = dict(checks=5)
        self.flann = cv2.FlannBasedMatcher(index_params, search_params)

        # orb
        self.orb = cv2.ORB_create()
        self.bf = cv2.BFMatcher()

        self.warp_size = None

        self.good_match_thresh = GOOD_MATCH_THRESH

    def _get_surf_features(self, img):
        # img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        kp, des = self.surf.detectAndCompute(img, None)
        return {'kp': kp, 'des': des}

    def _get_orb_features(self, img):
        kp = self.orb.detect(img, None)
        kp, des = self.orb.compute(img, kp)
        return {'kp': kp, 'des': des}

    def __get_matched_kps(self, img1, img2):
        # extract the key-points and descriptions
        _feature1 = self._get_surf_features(img=img1)
        _feature2 = self._get_surf_features(img=img2)

        # matching the key-points
        # matches = self.flann.knnMatch(feature1['des'], feature2['des'], k=2)
        matches = self.bf.knnMatch(_feature1['des'], _feature2['des'], k=2)

        # filter good matched key-points
        _goods = []
        for i, (m, n) in enumerate(matches):
            if m.distance < self.good_match_thresh * n.distance:
                _goods.append((m.trainIdx, m.queryIdx))

        _kps1 = _feature1['kp']
        _kps2 = _feature2['kp']

        # ---------------------------------------------------------------------------------------
        # rearrange teh the key points
        if len(_goods) < 5:
            return False, None, None
        else:
            matched_kps1 = [_kps1[i].pt for (_, i) in _goods]
            matched_kps2 = [_kps2[i].pt for (i, _) in _goods]

            return True, np.array(matched_kps1), np.array(matched_kps2)

    def get_upper_bottom_matched_kps(self, img1, img2):
        try:
            h, w = img1.shape[:2]

            upper_img1 = img1[: h * 3 // 7]
            upper_img2 = img2[: h * 3 // 7]

            # extract the key-points and descriptions
            upper_feature1 = self._get_surf_features(upper_img1)
            upper_feature2 = self._get_surf_features(upper_img2)

            # matching the key-points
            # matches = self.flann.knnMatch(feature1['des'], feature2['des'], k=2)
            matches = self.bf.knnMatch(upper_feature1['des'], upper_feature2['des'], k=2)

            # filter good matched key-points
            upper_goods = []
            for i, (m, n) in enumerate(matches):
                if m.distance < self.good_match_thresh * n.distance:
                    upper_goods.append((m.trainIdx, m.queryIdx))

            upper_kps1 = upper_feature1['kp']
            upper_kps2 = upper_feature2['kp']

            # bottom --------------------------------------------------------------------------------
            bottom_img1 = img1[h * 4 // 7:]
            bottom_img2 = img2[h * 4 // 7:]
            # extract the key-points and descriptions
            bottom_feature1 = self._get_surf_features(bottom_img1)
            bottom_feature2 = self._get_surf_features(bottom_img2)

            # matching the key-points
            # matches = self.flann.knnMatch(feature1['des'], feature2['des'], k=2)
            matches = self.bf.knnMatch(bottom_feature1['des'], bottom_feature2['des'], k=2)

            # filter good matched key-points
            bottom_goods = []
            for i, (m, n) in enumerate(matches):
                if m.distance < self.good_match_thresh * n.distance:
                    bottom_goods.append((m.trainIdx, m.queryIdx))

            bottom_kps1 = bottom_feature1['kp']
            bottom_kps2 = bottom_feature2['kp']

            # ---------------------------------------------------------------------------------------
            # rearrange teh the key points
            if len(upper_goods) < 5 or len(bottom_goods) < 5:
                sys.stdout.write("   {}, {}".format(len(upper_goods), len(bottom_goods)))
                return False, None, None
            else:
                matched_kps1 = [upper_kps1[i].pt for (_, i) in upper_goods]
                for (_, i) in bottom_goods:
                    pt = bottom_kps1[i].pt
                    matched_kps1.append((pt[0], pt[1] + 4 * h // 7))
                matched_kps1 = np.float32(matched_kps1)

                matched_kps2 = [upper_kps2[i].pt for (i, _) in upper_goods]
                for (i, _) in bottom_goods:
                    pt = bottom_kps2[i].pt
                    matched_kps2.append((pt[0], pt[1] + 4 * h // 7))
                matched_kps2 = np.float32(matched_kps2)

                # clear the matched key-points
                cleared_kps1, cleared_kps2 = self.remove_error_matches(matched_kps1, matched_kps2)

                if self.debug:
                    # for pt in matched_kps1:
                    #     img1 = cv2.circle(img1, (int(pt[0]), int(pt[1])), 5, (0, 0, 255), -1)
                    # for pt in matched_kps2:
                    #     img2 = cv2.circle(img2, (int(pt[0]), int(pt[1])), 5, (255, 0, 0), -1)

                    # cv2.imwrite(os.path.join(DEBUG_DIR, "key_points_{}.jpg".format(int(time.time()))), show)

                    show = np.hstack((img1, img2))
                    for i in range(len(matched_kps1)):
                        pt1 = matched_kps1[i]
                        pt2 = matched_kps2[i]
                        cv2.line(show, (int(pt1[0]), int(pt1[1])), (int(pt2[0] + w), int(pt2[1])), (255, 0, 0), 2)
                    cv2.imwrite(os.path.join(DEBUG_DIR, "matched_key_points.jpg".format(int(time.time()))), show)

                    show = np.hstack((img1, img2))
                    for i in range(len(cleared_kps1)):
                        pt1 = cleared_kps1[i]
                        pt2 = cleared_kps2[i]
                        cv2.line(show, (int(pt1[0]), int(pt1[1])), (int(pt2[0] + w), int(pt2[1])), (0, 0, 255), 2)
                    cv2.imwrite(os.path.join(DEBUG_DIR, "cleared_key_points.jpg".format(int(time.time()))), show)

                return True, cleared_kps1, cleared_kps2
        except Exception as e:
            print(e)
            return False, None, None

    @staticmethod
    def remove_error_matches(kps1, kps2):
        cleared_kps1 = []
        cleared_kps2 = []
        _avg_dis = 0
        for i in range(len(kps1)):
            pt1 = kps1[i]
            pt2 = kps2[i]
            _dis = math.sqrt((pt1[0] - pt2[0]) ** 2 + (pt1[1] - pt2[1]) ** 2)
            _avg_dis += _dis / len(kps1)

        for i in range(len(kps1)):
            pt1 = kps1[i]
            pt2 = kps2[i]
            _dis = math.sqrt((pt1[0] - pt2[0]) ** 2 + (pt1[1] - pt2[1]) ** 2)
            if _avg_dis * 0.65 < _dis < _avg_dis * 1.35:
                cleared_kps1.append(pt1)
                cleared_kps2.append(pt2)
            else:
                continue

        return cleared_kps1, cleared_kps2

    def match(self, l_img, r_img):

        h, w = l_img.shape[:2]

        # new warp size
        self.warp_size = (int(2 * w), h)

        # crop the half of images
        img1 = l_img[:, int(w * 3 / 4):int(w * (4 / 4))]
        img2 = r_img[:, :int(w * 1 / 4)]

        ret, matched_kps1, matched_kps2 = self.get_upper_bottom_matched_kps(img1=img1, img2=img2)
        # ret, matched_kps1, matched_kps2 = self.__get_matched_kps(img1=img1, img2=img2)
        if not ret:
            return False, None

        # homo_matrix_1, homo_matrix_2, cen_mid_pt = self.calculate_homograph_matrix(matched_kps1=matched_kps1,
        #                                                                            matched_kps2=matched_kps2,
        #                                                                            w=w)
        homo_matrix_1, homo_matrix_2, cen_mid_pt = self.calculate_homograph_matrix_ext(matched_kps1=matched_kps1,
                                                                                       matched_kps2=matched_kps2,
                                                                                       w=w)

        if homo_matrix_1 is not None and homo_matrix_2 is not None:
            ret = True
        else:
            ret = False
        return ret, {'left': homo_matrix_1, 'right': homo_matrix_2, 'center': cen_mid_pt}

    def calculate_homograph_matrix(self, matched_kps1, matched_kps2, w):
        # real position on the whole image
        real_kps1 = matched_kps1 + np.array([int(w*3/4), 0])
        real_kps2 = matched_kps2 + np.array([w, 0])

        # move to the center of screen with margin(w/2, h/2)
        mid_pts = real_kps1 * 0.5 + real_kps1 * 0.5

        # calculate the center of mit_pts
        cen_mid_pt = np.mean(mid_pts, axis=0)

        # calculate the homograph
        # left and right image to warpped plate
        homo_matrix_1 = self.calculate_transfer_matrix(pts1=real_kps1, pts2=mid_pts)
        homo_matrix_2 = self.calculate_transfer_matrix(pts1=matched_kps2, pts2=mid_pts)

        return homo_matrix_1, homo_matrix_2, cen_mid_pt

    def calculate_homograph_matrix_ext(self, matched_kps1, matched_kps2, w):
        # real position on the whole image
        real_kps1 = matched_kps1 + np.array([int(w * 3 / 4), 0])
        real_kps2 = matched_kps2 + np.array([w, 0])

        # move to the center of screen with margin(w/2, h/2)
        mid_pts = real_kps1 * 0.5 + real_kps2 * 0.5

        # calculate the center of mit_pts
        cen_mid_pt = np.mean(mid_pts, axis=0)

        # calculate the homograph
        # left and right image to warpped plate
        homo_matrix_1 = self.calculate_transfer_matrix(pts1=real_kps1, pts2=mid_pts)
        homo_matrix_2 = self.calculate_transfer_matrix(pts1=matched_kps2, pts2=mid_pts)

        # 2nd homograph transformation between matched kps2 to ext_kps
        ext_kps2 = []
        for pt in matched_kps2:
            ext_kps2.append(self.__pt_perspective_transfer(mat=homo_matrix_2, pt=pt))

        ext_homo_matrix_1 = self.calculate_transfer_matrix(pts1=real_kps1, pts2=ext_kps2)
        return ext_homo_matrix_1, homo_matrix_2, cen_mid_pt

    @staticmethod
    def __pt_perspective_transfer(mat, pt):
        _m = np.dot(np.array(mat), np.array([pt[0], pt[1], 1]))
        _tx, _ty, _t = _m[0], _m[1], _m[2]
        new_pt = np.array([_tx / _t, _ty / _t])
        return new_pt

    @staticmethod
    def transform(img, matrix, dst_sz, match_mode=MATCHING_MODE):
        if match_mode == PERSPECTIVE_MATCH:
            dst_img = cv2.warpPerspective(img, matrix, (dst_sz[0], dst_sz[1]))
        elif match_mode == AFFINE_MATCH:
            dst_img = cv2.warpAffine(img, matrix, (dst_sz[0], dst_sz[1]))
        else:
            dst_img = None
        return dst_img

    @staticmethod
    def calculate_transfer_matrix(pts1, pts2, match_mode=MATCHING_MODE):

        if match_mode == PERSPECTIVE_MATCH:
            matrix, _ = cv2.findHomography(np.array(pts1), np.array(pts2), method=MATCH_METHOD,
                                           ransacReprojThreshold=15)
        elif match_mode == AFFINE_MATCH:
            # matrix = cv2.getAffineTransform(pts1, pts2)
            matrix, _ = cv2.estimateAffine2D(np.array(pts1), np.array(pts2), method=MATCH_METHOD)
        else:
            matrix = None
        return matrix
