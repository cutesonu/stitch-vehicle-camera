import sys
import numpy as np
import cv2
import math
import os
import random
import json

from src.cylinder import CylinderModel
from src.homograph import HomographMatrix
from src.color_calibration import ColorCalibration
from utils.constants import IMG_EXT, MODEL_DIR, DEBUG_DIR, CONFIG_FILE, SRC_HEIGHT, SRC_WIDTH, N_SAMPLES, \
    MATCHING_MODE, PERSPECTIVE_MATCH, AFFINE_MATCH


class StitchConfig:

    def __init__(self, b_pre_calib=False, debug=True):

        self.height, self.width = SRC_HEIGHT, SRC_WIDTH
        self.n_samples = N_SAMPLES

        if b_pre_calib:
            # self.calib_model = CameraModel().get_cam_model()
            self.calib_model = CylinderModel().get_cylinder_model()
        else:
            self.calib_model = None

        self.warp_size = {'width': 2 * self.width,
                          'height': self.height}
        self.homograph = HomographMatrix(debug=debug)
        self.color_calib = ColorCalibration(warp_size=self.warp_size, m_cylinder_model=self.calib_model, debug=debug)

        self.debug = debug
        self.b_pre_calib = b_pre_calib

        self.debug = debug

        self.flt_sz = 21

    def collect_images(self, src_folder):
        """
            left: Series_010_Cam0_Seq_00326.jpg
            right: Series_009_Cam1_Seq_00006.jpg

            Series_[series_id]_[camera_id]_Seq_[seq_id].jpg

        :param src_folder:
        :return:
        """
        l_paths = []
        r_paths = []

        for fname in os.listdir(src_folder):
            ret, series_id, cam_id, seq_id, _ = self.parse_fname(fname=fname)

            if ret:
                if cam_id == 0:  # left camera
                    l_paths.append(os.path.join(src_folder, fname))
                else:  # 1: right camera
                    r_paths.append(os.path.join(src_folder, fname))

        left_paths = []
        right_paths = []
        # matched left_path, right_path
        for i in range(len(l_paths)):
            left_path = l_paths[i]
            folder, l_fname = os.path.split(left_path)
            # ext = os.path.splitext(l_fname)[1]
            _, series_id, cam_id, seq_id, _ = self.parse_fname(fname=l_fname)

            # r_fname = "Series_{:03d}_{}_Seq_{:05}{}".format(series_id, "Cam1", seq_id, ext)
            r_fname = l_fname.replace("Cam0", "Cam1")
            right_path = os.path.join(folder, r_fname)

            if right_path not in r_paths:
                print("could not find the paired right image for {}".format(l_fname))
                continue

            left_paths.append(left_path)
            right_paths.append(right_path)

        return left_paths, right_paths

    def __calculate_homograph(self, left_paths, right_paths):
        n_samples = min(self.n_samples, len(left_paths), len(right_paths))

        _min_sz = min(len(left_paths), len(right_paths))
        total_ids = list(range(_min_sz))
        random.shuffle(total_ids)

        sample_ids = total_ids[:]

        if MATCHING_MODE == PERSPECTIVE_MATCH:
            # perspective transform
            left_homograph_matrix = np.zeros((3, 3), dtype=np.float32)
            right_homograph_matrix = np.zeros((3, 3), dtype=np.float32)

        elif MATCHING_MODE == AFFINE_MATCH:
            # affine
            left_homograph_matrix = np.zeros((2, 3), dtype=np.float32)
            right_homograph_matrix = np.zeros((2, 3), dtype=np.float32)
        else:
            print("unknown matching mode")
            sys.exit(1)

        center_point = np.zeros((1, 2), dtype=np.float32)

        cnt_pos = 0
        for _idx, i in enumerate(sample_ids):
            sys.stdout.write("\r\t\t{}/{}  ".format(_idx + 1, len(sample_ids)))
            left_path = left_paths[i]
            right_path = right_paths[i]

            # validate the image size
            left_img = cv2.imread(left_path)
            right_img = cv2.imread(right_path)

            if left_img is None or right_img is None:
                print("failed to read images, left:{} right:{}".format((left_img is not None), (right_img is not None)))
                continue

            if left_img.shape != right_img.shape or left_img.shape[:2] != tuple([self.height, self.width]):
                print("not matched image size, should be {}, but got {}".format([self.height, self.width],
                                                                                (left_img.shape[:2])))
                continue

            # camera lens calibration
            if self.b_pre_calib:
                left_calib = cv2.remap(left_img, self.calib_model['map1'], self.calib_model['map2'], cv2.INTER_CUBIC)
                right_calib = cv2.remap(right_img, self.calib_model['map1'], self.calib_model['map2'], cv2.INTER_CUBIC)
            else:
                left_calib = left_img.copy()
                right_calib = right_img.copy()

            # calculate homograph matrix
            ret, homograph_dict = self.homograph.match(l_img=left_calib, r_img=right_calib)

            if not ret:
                print("failed matching")
                continue
            else:
                left_homograph_matrix += homograph_dict['left']
                right_homograph_matrix += homograph_dict['right']
                center_point += homograph_dict['center']
                cnt_pos += 1

            if cnt_pos == N_SAMPLES:
                break

        print()
        if cnt_pos != 0:
            left_homograph_matrix /= cnt_pos
            right_homograph_matrix /= cnt_pos
            center_point /= cnt_pos
        else:
            print("failed to calculate the stitching model")
            sys.exit(0)

        return left_homograph_matrix, right_homograph_matrix, center_point

    @staticmethod
    def parse_fname(fname):
        """
            Series_009_Cam1_Seq_00006.jpg """
        suc = False
        msg = ""
        series_id = -1
        cam_id = -1
        seq_id = -1
        try:
            base, ext = os.path.splitext(fname)
            if ext != IMG_EXT:
                msg = "no image file"

            sps = base.split("_")
            if sps[0].startswith('Series') and sps[2].startswith('Cam') and sps[3].startswith('Seq'):
                series_id = int(sps[1])
                cam_id = int(sps[2][-1])
                seq_id = int(sps[4])
                suc = True
            else:
                suc = False
                msg = "failed"
        except Exception as e:
            msg = str(e)

        return suc, series_id, cam_id, seq_id, msg

    def __generate_mask(self, h_mat_1, h_mat_2, cen_pt):
        """ ####### mask for frame ######################################################### """
        w = self.width
        h = self.height

        cen_pt = cen_pt[0]

        # create the np.ones
        ones = np.ones((h, w, 3), dtype=np.uint8) * 255
        if self.b_pre_calib:
            # camera distort
            ones_calib_lefts = cv2.remap(src=ones, map1=self.calib_model['map1'], map2=self.calib_model['map2'],
                                         interpolation=cv2.INTER_CUBIC)
            ones_calib_rights = cv2.remap(src=ones, map1=self.calib_model['map1'], map2=self.calib_model['map2'],
                                          interpolation=cv2.INTER_CUBIC)
        else:
            ones_calib_lefts = ones.copy()
            ones_calib_rights = ones.copy()

        # RGB to Gray
        ones_calib_left = cv2.cvtColor(ones_calib_lefts, cv2.COLOR_BGR2GRAY)
        ones_calib_right = cv2.cvtColor(ones_calib_rights, cv2.COLOR_BGR2GRAY)

        # warpping
        ones_warp_left = self.homograph.transform(img=ones_calib_left, matrix=h_mat_1,
                                                  dst_sz=(self.warp_size['width'], self.warp_size['height']))
        ones_warp_right = self.homograph.transform(img=ones_calib_right, matrix=h_mat_2,
                                                   dst_sz=(self.warp_size['width'], self.warp_size['height']))

        # create binary image
        _, left_mask = cv2.threshold(ones_warp_left, 1, 255, cv2.THRESH_BINARY)
        _, right_mask = cv2.threshold(ones_warp_right, 1, 255, cv2.THRESH_BINARY)

        if self.debug:
            cv2.imwrite(os.path.join(DEBUG_DIR, "left_mask.jpg"), left_mask)
            cv2.imwrite(os.path.join(DEBUG_DIR, "right_mask.jpg"), right_mask)

        """ ####### mask for half ######################################################### """
        half = np.zeros((self.warp_size['height'], self.warp_size['width']), dtype=np.uint8)
        half[:, :int(cen_pt[0])] = np.ones((half.shape[0], int(cen_pt[0])), dtype=np.uint8) * 255

        """ ### optimize the crop rect ############################################################# """
        # calculate the optimized crop size
        opt_crop_rect = self.__calculate_optimized_crop_size(left_mask, right_mask, cen_pt)

        # update the warp_size
        self.warp_size['width'] = int(opt_crop_rect['right'] - opt_crop_rect['left'])
        self.warp_size['height'] = int(opt_crop_rect['bottom'] - opt_crop_rect['top'])

        left_mask = cv2.bitwise_and(left_mask, half)
        kernel = self.flt_sz
        blur_mask = cv2.GaussianBlur(left_mask, (kernel, kernel), 0)
        # blur_mask = cv2.cvtColor(blur_mask, cv2.COLOR_GRAY2BGR)

        opt_mask_crop = blur_mask[int(opt_crop_rect['top']):int(opt_crop_rect['bottom']),
                                  opt_crop_rect['left']:opt_crop_rect['right']]

        # opt_mask = opt_mask_crop.astype(np.float32)  # float
        opt_mask = opt_mask_crop.copy()  # uint8

        if self.debug:
            # cv2.imshow("opt_mask", cv2.resize(opt_mask_crop, None, fx=0.2, fy=0.2))
            # cv2.waitKey(1)
            cv2.imwrite(os.path.join(DEBUG_DIR, "opt_mask.jpg"), opt_mask_crop)

        return opt_mask, opt_crop_rect

    @staticmethod
    def __calculate_optimized_crop_size(left_mask, right_mask, c_pt):

        # find the max size contour on left mask image
        _, contour_left, _ = cv2.findContours(left_mask, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        max_rect = 0.0
        l_c_pts = None
        for c in contour_left:
            (x, y, w, h) = cv2.boundingRect(c)
            if max_rect == 0.0:
                max_rect = math.fabs(w * h)
                l_c_pts = c
            else:
                if max_rect < math.fabs(w * h):
                    max_rect = math.fabs(w * h)
                    l_c_pts = c

        # find the max size contour on right mask image
        _, contour_right, _ = cv2.findContours(right_mask, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        max_rect = 0.0
        r_c_pts = None
        for c in contour_right:
            (x, y, w, h) = cv2.boundingRect(c)
            if max_rect == 0.0:
                max_rect = math.fabs(w * h)
                r_c_pts = c
            else:
                if max_rect < math.fabs(w * h):
                    max_rect = math.fabs(w * h)
                    r_c_pts = c

        if l_c_pts is not None and r_c_pts is not None:

            vertical = []
            horizon = []
            for i in range(len(l_c_pts) - 1):
                if i == (len(l_c_pts) - 1):
                    pt_i = l_c_pts[i][0]
                    pt_i_1 = l_c_pts[0][0]
                else:
                    pt_i = l_c_pts[i][0]
                    pt_i_1 = l_c_pts[i + 1][0]

                if pt_i[0] >= c_pt[0] >= pt_i_1[0] or pt_i[0] <= c_pt[0] <= pt_i_1[0]:
                    vertical.append(pt_i[1])
                if pt_i[1] >= c_pt[1] >= pt_i_1[1] or pt_i[1] <= c_pt[1] <= pt_i_1[1]:
                    horizon.append(pt_i[0])

            if len(vertical) == 0:
                l_top = 0
                l_bottom = left_mask.shape[0]
            else:
                l_top = min(vertical)
                l_bottom = max(vertical)
            if len(horizon) == 0:
                l_left = 0
            else:
                l_left = min(horizon)

            vertical = []
            horizon = []
            for i in range(len(r_c_pts)):
                if i == (len(r_c_pts) - 1):
                    pt_i = r_c_pts[i][0]
                    pt_i_1 = r_c_pts[0][0]
                else:
                    pt_i = r_c_pts[i][0]
                    pt_i_1 = r_c_pts[i + 1][0]

                if pt_i[0] >= c_pt[0] >= pt_i_1[0] or pt_i[0] <= c_pt[0] <= pt_i_1[0]:
                    vertical.append(pt_i[1])
                if pt_i[1] >= c_pt[1] >= pt_i_1[1] or pt_i[1] <= c_pt[1] <= pt_i_1[1]:
                    horizon.append(pt_i[0])

            if len(vertical) == 0:
                r_top = 0
                r_bottom = right_mask.shape[0]
            else:
                r_top = min(vertical)
                r_bottom = max(vertical)
            if len(horizon) == 0:
                r_right = right_mask.shape[1]
            else:
                r_right = max(horizon)

            crop_rect = {
                'left': int(l_left),
                'right': int(r_right),
                'top': int(max(l_top, r_top)),
                'bottom': int(min(l_bottom, r_bottom))
            }

            return crop_rect
        else:
            return None

    def validate_homograph(self, left_paths, right_paths, cen_pt, l_h_mat, r_h_mat):
        w, h = self.width, self.height
        mask = np.zeros((h, w * 2), dtype=np.uint8)
        mask[:, :int(cen_pt[0][0])] = 255

        n_samples = min(self.n_samples, len(left_paths), len(right_paths))
        _min_sz = min(len(left_paths), len(right_paths))
        total_ids = list(range(_min_sz))
        # random.shuffle(total_ids)
        sample_ids = total_ids[:n_samples]

        for _idx, i in enumerate(sample_ids):
            sys.stdout.write("\r\t\t{}/{}".format(_idx + 1, n_samples))
            sys.stdout.flush()

            left_path = left_paths[i]
            right_path = right_paths[i]

            left_img = cv2.imread(left_path)
            right_img = cv2.imread(right_path)

            if self.b_pre_calib:
                # camera distort
                left_calib = cv2.remap(src=left_img, map1=self.calib_model['map1'], map2=self.calib_model['map2'],
                                       interpolation=cv2.INTER_CUBIC)
                right_calib = cv2.remap(src=right_img, map1=self.calib_model['map1'], map2=self.calib_model['map2'],
                                        interpolation=cv2.INTER_CUBIC)
                cv2.imwrite(os.path.join(DEBUG_DIR, "validate_left_calib.jpg".format(i)), left_calib)
                cv2.imwrite(os.path.join(DEBUG_DIR, "validate_right_calib.jpg".format(i)), right_calib)
            else:
                left_calib = left_img.copy()
                right_calib = right_img.copy()

            # draw keypoints
            img1 = left_calib[:, int(w * 3 / 4):int(w * (4 / 4))]
            img2 = right_calib[:, :int(w * 1 / 4)]
            ret, matched_kps1, matched_kps2 = self.homograph.get_upper_bottom_matched_kps(img1=img1, img2=img2)
            real_kps1 = matched_kps1 + np.array([int(w * 3 / 4), 0])
            real_kps2 = matched_kps2

            for pt in real_kps1:
                cv2.circle(left_calib, (int(pt[0]), int(pt[1])), 10, (0, 0, 255), -1)
            for pt in real_kps2:
                cv2.circle(right_calib, (int(pt[0]), int(pt[1])), 10, (255, 0, 0), -1)

            # warp
            left_warp = self.homograph.transform(img=left_calib, matrix=l_h_mat, dst_sz=(w * 2, h))
            right_warp = self.homograph.transform(img=right_calib, matrix=r_h_mat, dst_sz=(w * 2, h))

            stitched = cv2.addWeighted(left_warp, 0.5, right_warp, 0.5, 0.0)
            # stitched = left_warp * left_mask + right_warp * right_mask

            stitched = stitched.astype(np.uint8)

            cv2.imwrite(os.path.join(DEBUG_DIR, "validate_{}.jpg".format(i)), stitched)
        print()

    def generate_config(self, kpts_folder, b_validate=False):
        left_paths, right_paths = self.collect_images(src_folder=kpts_folder)
        #
        #
        print("  Calculate the homograph matrix")
        l_h_mat, r_h_mat, cen_pt = self.__calculate_homograph(left_paths=left_paths, right_paths=right_paths)
        #
        #
        print("  Create the mask image")
        gray_mask_uint, opt_crop_rect = self.__generate_mask(h_mat_1=l_h_mat, h_mat_2=r_h_mat, cen_pt=cen_pt)
        warp_width = gray_mask_uint.shape[1]
        warp_height = gray_mask_uint.shape[0]

        #
        # color calibration
        print("  Calculate the Color calibration coefficients")
        color_ratios = self.color_calib.calculate_color_calib_ratio(left_paths=left_paths, right_paths=right_paths,
                                                                    l_h_mat=l_h_mat,
                                                                    r_h_mat=r_h_mat)

        #
        #
        if self.debug and b_validate:
            print("  Validate")
            self.validate_homograph(left_paths=left_paths, right_paths=right_paths, cen_pt=cen_pt, l_h_mat=l_h_mat,
                                    r_h_mat=r_h_mat)
            # sys.exit(0)

        #
        #
        print("  Write the config.json")
        mask_path = os.path.join(MODEL_DIR, 'mask.npy')
        config_dict = {
            'left_homo_matrix': l_h_mat.tolist(),
            'right_homo_matrix': r_h_mat.tolist(),
            'mask_path': 'mask.npy',
            'warp_size': [warp_width, warp_height],
            'color_ratios': color_ratios.tolist(),
            'center_point': cen_pt.tolist(),
            'crop_rect': opt_crop_rect

        }
        with open(CONFIG_FILE, 'w') as jp:
            json.dump(config_dict, jp, indent=4, sort_keys=True)

        #
        #
        print("  Save mask image")
        np.save(mask_path, gray_mask_uint)

        return config_dict


if __name__ == '__main__':
    from utils.constants import ROOT_DIR

    gen_config = StitchConfig(b_pre_calib=True, debug=True)

    _src_folder = os.path.join(ROOT_DIR, os.pardir, "images/key_points")
    gen_config.generate_config(kpts_folder=_src_folder, b_validate=True)
