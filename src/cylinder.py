import numpy as np
import math
import cv2

from utils.constants import SRC_WIDTH, SRC_HEIGHT, CYLINDER_FOCAL_LEN


class CylinderModel:
    def __init__(self, focus_len=CYLINDER_FOCAL_LEN, scale=1.0):
        self.focus_len = focus_len
        self.scale = scale
        self.width, self.height = SRC_WIDTH, SRC_HEIGHT
        self.cylinder_model = self.__build_modle()

    def get_cylinder_model(self):
        return self.cylinder_model

    def __build_modle(self):
        print("Config Cylinder Model")
        f, s = self.focus_len, self.scale
        width, height = self.width, self.height

        map_x = np.zeros((height, width), dtype=np.float32)
        map_y = np.zeros((height, width), dtype=np.float32)

        cx = width / 2.
        cy = height / 2.

        for y in range(height):
            fy = float(y)
            for x in range(width):
                fx = float(x)
                # float new_x = input_size.width * std::atan((fx - cx) / f) + cx
                # float new_y = input_size.height * (fy - cy) / std::sqrt(std::pow(fx - cx, 2) + std::pow(f, 2)) + cy

                new_x = f * math.tan((fx - cx) / (f * s)) + cx
                new_y = (f / (f * s)) * (fy - cy) / math.cos((fx - cx) / (f * s)) + cy

                map_x[y][x] = new_x
                map_y[y][x] = new_y

        return {'map1': map_x, 'map2': map_y}


if __name__ == '__main__':
    import os
    source_folder = "../../images/best"
    path1 = os.path.join(source_folder, "Series_002_Cam0_Seq_00004.jpg")
    path2 = os.path.join(source_folder, "Series_002_Cam1_Seq_00004.jpg")

    focal_length = 2400
    output_folder = "../../images/cylinder_{}".format(focal_length)
    os.mkdir(output_folder)

    model = CylinderModel(focus_len=focal_length, scale=1.0).get_cylinder_model()

    for fn in os.listdir(source_folder):
        path = os.path.join(source_folder, fn)
        img = cv2.imread(path)
        cylinder = cv2.remap(img, model['map1'], model['map2'], cv2.INTER_LINEAR)

        outpath = os.path.join(output_folder, fn)
        cv2.imwrite(outpath, cylinder)
