import cv2
import numpy as np
import sys

from utils.constants import SRC_WIDTH, SRC_HEIGHT


class CameraModel:

    model = None

    def __init__(self, scale=0.65):

        self.scale_range_min = 0.6
        self.scale_range_max = 1.0
        self.scale_range_step = 0.05

        self.model = self._init_undistort_model(SRC_WIDTH, SRC_HEIGHT, scale)

    def _init_undistort_model(self, width, height, scale):

        cen_x = width // 2
        cen_y = height // 2

        """ ------------- Camera Calibration Parameters -----------------"""
        K = np.array([[1700, 0., cen_x],
                      [0., 1100, cen_y],  # [0., 750, cy], origin
                      [0., 0., 1.]])

        D = np.array([0., 0., 0., 0.])

        scale_factor = 0.65
        if scale is not None and self.scale_range_min <= scale <= self.scale_range_max:
            scale_factor = scale
        else:
            # find the optimize scale factor
            if scale is None:
                sys.stdout.write("\n\tScan the scale factor from [{}, {}] with step {}\n".format(self.scale_range_min,
                                                                                                 self.scale_range_max,
                                                                                                 self.scale_range_step))
            elif scale is not None and not self.scale_range_min <= scale <= self.scale_range_max:
                sys.stdout.write("\n\tOut of range Scale factor {}, Scan the proper scale value\n".format(scale))

            # find the optimize scale factor
            ones = np.ones((height, width, 3), dtype=np.uint8) * 255
            _step = self.scale_range_step
            _min = self.scale_range_min
            _max = self.scale_range_max

            for scale in np.arange(_min, _max, _step):
                sys.stdout.write("\r\tscale : {}".format(scale))
                sys.stdout.flush()

                Knew = K.copy()
                Knew[(0, 1), (0, 1)] *= scale
                _map1, _map2 = cv2.fisheye.initUndistortRectifyMap(K=K, D=D, R=None, P=Knew, size=(width, height),
                                                                   m1type=cv2.CV_16SC2)
                # camera distort
                ones_calib = cv2.remap(src=ones, map1=_map1, map2=_map2, interpolation=cv2.INTER_LINEAR)

                # RGB to Gray
                ones_calib_gray = cv2.cvtColor(ones_calib, cv2.COLOR_BGR2GRAY)

                # create binary image
                mask = cv2.threshold(ones_calib_gray, 1, 255, cv2.THRESH_BINARY)[1]

                if mask[cen_y][0] == 255 and mask[cen_y][-1] == 255:
                    scale_factor = scale - _step
                    break

        sys.stdout.write("\tOptimized Scale factor : {}\n".format(scale_factor))

        Knew = K.copy()
        Knew[(0, 1), (0, 1)] *= scale_factor
        map1, map2 = cv2.fisheye.initUndistortRectifyMap(K=K, D=D, R=None, P=Knew, size=(width, height),
                                                         m1type=cv2.CV_16SC2)
        return {'map1': map1, 'map2': map2}

    def get_cam_model(self):
        return self.model
