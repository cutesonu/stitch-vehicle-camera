import os
import cv2
import sys
import numpy as np
import json


from src.stitch_config import StitchConfig
from utils.constants import CONFIG_FILE, ROOT_DIR, RESULT_DIR, N_SAMPLES, SRC_WIDTH, SRC_HEIGHT, MODEL_DIR


class Stitch:
    def __init__(self, src_folder, kpts_folder, b_color_calib=True, b_pre_calib=True, b_save=True):

        self.src_folder = src_folder
        self.kpts_folder = kpts_folder
        self.n_samples = N_SAMPLES
        self.width = SRC_WIDTH
        self.height = SRC_HEIGHT
        self.b_pre_calib = b_pre_calib
        self.b_save = b_save
        self.b_color_calib = b_color_calib

        self.gen_config = StitchConfig(b_pre_calib=b_pre_calib, debug=True)
        self.calib_model = self.gen_config.calib_model

    def load_config(self):
        try:
            with open(CONFIG_FILE, 'r') as jp:
                stitch_config = json.load(jp)
        except Exception as e:
            print(str(e))
            # calculate stitching models
            stitch_config = self.gen_config.generate_config(kpts_folder=self.kpts_folder, b_validate=False)

        mask_path = os.path.join(MODEL_DIR, stitch_config['mask_path'])
        mask = np.load(mask_path)
        stitch_config['mask'] = mask

        return stitch_config

    @staticmethod
    def generate_color_calib_mask(left_mask, right_mask, stitch_config, color_calib_mode="constant"):
        color_ratios = stitch_config['color_ratios']
        center_point = stitch_config['center_point']
        warp_w, warp_h = stitch_config['warp_size']

        if color_calib_mode == "constant":
            b1, g1, r1 = cv2.split(left_mask)
            b2, g2, r2 = cv2.split(right_mask)

            b1 = b1 * color_ratios[0][0]
            g1 = g1 * color_ratios[0][1]
            r1 = r1 * color_ratios[0][2]

            b2 = b2 * color_ratios[1][0]
            g2 = g2 * color_ratios[1][1]
            r2 = r2 * color_ratios[1][2]

        else:  # "gradient":
            # gradient color calibration
            x0 = center_point[0][0]
            width, height = warp_w, warp_h

            """ left gradient mask """
            b1, g1, r1 = cv2.split(left_mask)
            delta_b1 = (1. - color_ratios[0][0]) / (.0 - x0)
            delta_g1 = (1. - color_ratios[0][1]) / (.0 - x0)
            delta_r1 = (1. - color_ratios[0][2]) / (.0 - x0)

            b2, g2, r2 = cv2.split(right_mask)
            delta_b2 = (color_ratios[1][0] - 1.) / (x0 - width)
            delta_g2 = (color_ratios[1][1] - 1.) / (x0 - width)
            delta_r2 = (color_ratios[1][2] - 1.) / (x0 - width)

            for x in range(0, width):
                sys.stdout.write("\r\t {} / {}".format(x, width))
                sys.stdout.flush()
                for y in range(height):
                    b1[y][x] = b1[y][x] * (1. + delta_b1 * x)
                    g1[y][x] = g1[y][x] * (1. + delta_g1 * x)
                    r1[y][x] = r1[y][x] * (1. + delta_r1 * x)

                    b2[y][x] = b2[y][x] * (color_ratios[1][0] + delta_b2 * (x - x0))
                    g2[y][x] = g2[y][x] * (color_ratios[1][1] + delta_g2 * (x - x0))
                    r2[y][x] = r2[y][x] * (color_ratios[1][2] + delta_r2 * (x - x0))

        # upgrade the mask
        _new_mask = cv2.merge((b1, g1, r1))
        left_mask = _new_mask

        _new_mask = cv2.merge((b2, g2, r2))
        right_mask = _new_mask

        return left_mask, right_mask

    def __matrix_to_map(self, mat, crop_rect):
        """
        input params:
            mat: transformation matrix
             which calculated by findHomography() or getPerspectTransformation()
            size: frame size (height, width)
             source and dest framesize
        output params:
            map_x, map_y: map matrix which is to be used on remap()
        """
        offset_x, offset_y = crop_rect['left'], crop_rect['top']
        warp_width, warp_height = crop_rect['right'] - crop_rect['left'], crop_rect['bottom'] - crop_rect['top']
        print("Offset:", offset_x, offset_y)

        width = self.width * 2
        height = self.height
        trans_mat = mat  # transformation matrix

        inv_trans_mat = trans_mat.copy()
        cv2.invert(trans_mat, inv_trans_mat)

        # generate the warp matrix
        src_trans_mat = inv_trans_mat.copy()
        map_x = np.zeros((height, width), dtype=np.float32)
        map_y = np.zeros((height, width), dtype=np.float32)

        m11 = src_trans_mat[0][0]
        m12 = src_trans_mat[0][1]
        m13 = src_trans_mat[0][2]
        m21 = src_trans_mat[1][0]
        m22 = src_trans_mat[1][1]
        m23 = src_trans_mat[1][2]
        m31 = src_trans_mat[2][0]
        m32 = src_trans_mat[2][1]
        m33 = src_trans_mat[2][2]

        for y in range(height):
            sys.stdout.write("\r\t\ty / height : {} / {}".format(y, height))
            sys.stdout.flush()
            fy = float(y)
            for x in range(width):
                fx = float(x)
                w = m31 * fx + m32 * fy + m33
                if w != 0.0:
                    w = 1.0 / w

                new_x = float((m11 * fx) + (m12 * fy) + m13) * w
                new_y = float((m21 * fx) + (m22 * fy) + m23) * w
                map_x[y][x] = new_x
                map_y[y][x] = new_y

        print()
        # create the fixed-point representation of the mapping resulting from map_x, map_y
        transform_x, transform_y = cv2.convertMaps(map1=map_x, map2=map_y, dstmap1type=cv2.CV_16SC2)

        offset_matrix = np.float32(
            [[1, 0, -offset_x],
             [0, 1, -offset_y]]
        )
        transform_x = cv2.warpAffine(transform_x, offset_matrix, (warp_width, warp_height))
        transform_y = cv2.warpAffine(transform_y, offset_matrix, (warp_width, warp_height))
        return {'map1': transform_x, 'map2': transform_y}

    def create_map_data(self, stitch_config, b_save_map=True):
        left_homo_mat = np.array(stitch_config['left_homo_matrix'])
        right_homo_mat = np.array(stitch_config['right_homo_matrix'])
        opt_crop_rect = stitch_config['crop_rect']

        #
        #
        print("  Convert the matrix to map")
        print("\tgenerate left map")
        _left_warp_maps = self.__matrix_to_map(mat=left_homo_mat, crop_rect=opt_crop_rect)

        print("\tgenerate right map")
        _right_warp_maps = self.__matrix_to_map(mat=right_homo_mat, crop_rect=opt_crop_rect)

        if self.b_pre_calib:
            print("  Combine the maps (camera + warp)")
            # calib_map1, calib_map2 = self.calib_model['map1'], self.calib_model['map2']
            calib_map1, calib_map2 = self.calib_model['map1'], self.calib_model['map2']
            left_warp_map1, left_warp_map2 = _left_warp_maps['map1'], _left_warp_maps['map2']
            right_warp_map1, right_warp_map2 = _right_warp_maps['map1'], _right_warp_maps['map2']

            # combine the camera with left_warp
            left_map1 = cv2.remap(src=calib_map1, map1=left_warp_map1, map2=left_warp_map2,
                                  interpolation=cv2.INTER_LINEAR)
            left_map2 = cv2.remap(src=calib_map2, map1=left_warp_map1, map2=left_warp_map2,
                                  interpolation=cv2.INTER_LINEAR)

            # combine the camera with right_warp
            right_map1 = cv2.remap(src=calib_map1, map1=right_warp_map1, map2=right_warp_map2,
                                   interpolation=cv2.INTER_LINEAR)
            right_map2 = cv2.remap(src=calib_map2, map1=right_warp_map1, map2=right_warp_map2,
                                   interpolation=cv2.INTER_LINEAR)

            left_warp_maps = {'map1': left_map1, 'map2': left_map2}
            right_warp_maps = {'map1': right_map1, 'map2': right_map2}

        else:
            left_warp_maps, right_warp_maps = _left_warp_maps, _right_warp_maps

        #
        # save stitching config
        if b_save_map:
            print("  Save the map data")
            left_warp_map1_path = os.path.join(MODEL_DIR, 'left_warp_map1.npy')
            left_warp_map2_path = os.path.join(MODEL_DIR, 'left_warp_map2.npy')
            np.save(left_warp_map1_path, left_warp_maps['map1'])
            np.save(left_warp_map2_path, left_warp_maps['map2'])

            right_warp_map1_path = os.path.join(MODEL_DIR, 'right_warp_map1.npy')
            right_warp_map2_path = os.path.join(MODEL_DIR, 'right_warp_map2.npy')
            np.save(right_warp_map1_path, right_warp_maps['map1'])
            np.save(right_warp_map2_path, right_warp_maps['map2'])

        return left_warp_maps, right_warp_maps

    def stitching(self):
        print("Collect the images")
        left_paths, right_paths = self.gen_config.collect_images(src_folder=self.src_folder)

        print("Load config")
        stitch_config = self.load_config()

        print("Create map data")
        left_warp_maps, right_warp_maps = self.create_map_data(stitch_config=stitch_config, b_save_map=True)

        print("Color calibration")
        mask = stitch_config['mask'].copy()

        left_mask = cv2.cvtColor(mask, cv2.COLOR_GRAY2BGR)
        left_mask = np.true_divide(left_mask, 255.0)
        right_mask = cv2.cvtColor((255 - mask), cv2.COLOR_GRAY2BGR)
        right_mask = np.true_divide(right_mask, 255.0)
        if self.b_color_calib:
            left_mask, right_mask = self.generate_color_calib_mask(stitch_config=stitch_config,
                                                                   left_mask=left_mask, right_mask=right_mask)

        print("Stitching...")
        # scanning the images
        for i, left_path in enumerate(left_paths):
            sys.stdout.write("\r {} / {}".format(i, len(left_paths)))
            right_path = right_paths[i]

            # validate teh image size
            left_img = cv2.imread(left_path)
            right_img = cv2.imread(right_path)

            if left_img is None or right_img is None:
                print("  failed to read images, left:{} right:{}".format((left_img is not None),
                                                                         (right_img is not None)))
                continue

            if left_img.shape != right_img.shape or left_img.shape[:2] != tuple([self.height, self.width]):
                print("  not matched image size, should be {}, but got {}".format([self.height, self.width],
                                                                                  (left_img.shape[:2])))
                continue

            # warp
            left_trans = cv2.remap(src=left_img, map1=left_warp_maps['map1'], map2=left_warp_maps['map2'],
                                   interpolation=cv2.INTER_CUBIC)
            right_trans = cv2.remap(src=right_img, map1=right_warp_maps['map1'], map2=right_warp_maps['map2'],
                                    interpolation=cv2.INTER_CUBIC)
            stitched = left_trans * left_mask + right_trans * right_mask
            stitched = stitched.astype(np.uint8)

            if self.b_save:
                # saver.write(stitched)
                # cv2.imshow("stitched", stitched)
                cv2.imwrite(os.path.join(RESULT_DIR, "stitched_{}.jpg".format(i)), stitched)
                cv2.waitKey(1)
        sys.stdout.write("\n")

        print("Finished the Stitching")


if __name__ == '__main__':
    _src_folder = os.path.join(ROOT_DIR, os.pardir, "images/key_points")
    _key_points = os.path.join(ROOT_DIR, os.pardir, "images/key_points")

    Stitch(src_folder=_src_folder, kpts_folder=_key_points).stitching()
