
import os
import cv2

# --------------------------------------- [PATHS] ----------------------------------------------------------------------
_cur_dir = os.path.dirname(os.path.realpath(__file__))
# ROOT
ROOT_DIR = os.path.join(_cur_dir, os.pardir)

MODEL_DIR = os.path.join(ROOT_DIR, 'models')
RESULT_DIR = os.path.join(ROOT_DIR, "result")
DEBUG_DIR = os.path.join(ROOT_DIR, "debug")

for folder in [RESULT_DIR, DEBUG_DIR, MODEL_DIR]:
    if not os.path.exists(folder):
        os.mkdir(folder)

CONFIG_FILE = os.path.join(MODEL_DIR, 'config.json')


# --------------------------------------- [CONSTANTS] ------------------------------------------------------------------
IMG_EXT = ".jpg"

SRC_WIDTH = 2448
SRC_HEIGHT = 2048
N_SAMPLES = 10

PERSPECTIVE_MATCH = "PERSPECTIVE_MATCH"
AFFINE_MATCH = "AFFINE_MATCH"
MATCHING_MODE = PERSPECTIVE_MATCH

# MATCH_METHOD = cv2.LMEDS  #
MATCH_METHOD = cv2.RANSAC  #

GOOD_MATCH_THRESH = 0.4

CYLINDER_FOCAL_LEN = 2200
